package org.mkleint.netbeans.index.properties;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Pattern;
import javax.swing.Icon;
import javax.swing.text.BadLocationException;
import org.netbeans.api.java.classpath.ClassPath;
import org.netbeans.api.java.classpath.GlobalPathRegistry;
import org.netbeans.api.project.FileOwnerQuery;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ProjectUtils;
import org.netbeans.editor.BaseDocument;
import org.netbeans.editor.FinderFactory;
import org.netbeans.editor.Utilities;
import org.netbeans.modules.parsing.spi.indexing.support.IndexResult;
import org.netbeans.modules.parsing.spi.indexing.support.QuerySupport;
import org.netbeans.spi.jumpto.symbol.SymbolDescriptor;
import org.netbeans.spi.jumpto.symbol.SymbolProvider;
import org.netbeans.spi.jumpto.type.SearchType;
import org.openide.cookies.LineCookie;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.text.CloneableEditorSupport;
import org.openide.text.Line;
import org.openide.util.Exceptions;
import org.openide.util.lookup.ServiceProvider;

@ServiceProvider(service = SymbolProvider.class)
public class PropertiesSymbolProvider implements SymbolProvider{
    private final AtomicBoolean cancelled = new AtomicBoolean(false);

    @Override
    public String name() {
        return "property-keys";
    }

    @Override
    public String getDisplayName() {
        return "Property keys";
    }

    @Override
    public void computeSymbolNames(Context context, Result result) {
        cancelled.set(false);

        SearchType type = context.getSearchType();

        Collection<FileObject> roots = getIndexerRoots(context.getProject());
        if (cancelled.get()) return;
        try {
            String text = context.getText();
            if (SearchType.CAMEL_CASE == type) {
                StringBuilder sb = new StringBuilder();
                boolean prependSep = false;
                for (char ch : text.toCharArray()) {
                    if (Character.isUpperCase(ch)) {
                        if (prependSep) {
                            sb.append("*\\.");
                        } else {
                            prependSep = true;
                        }
                    }
                    sb.append(Character.toLowerCase(ch));
                }
                sb.append("*");
                text = sb.toString();
                type = SearchType.CASE_INSENSITIVE_REGEXP;

            }
            //case insensitive prefix with type uppercase letter doesn't work. why?
//            if (SearchType.CASE_INSENSITIVE_EXACT_NAME == type || SearchType.CASE_INSENSITIVE_PREFIX == type) {
//                text = text.toLowerCase();
//            }
            if (SearchType.CASE_INSENSITIVE_REGEXP == type  || SearchType.REGEXP == type) {
                text = text.replace("*", ".*").replace("?", ".?");
            }
            QuerySupport qs = QuerySupport.forRoots(PropertiesIndexerFactory.INDEXER_NAME, PropertiesIndexerFactory.INDEXER_VERSION, roots.toArray(new FileObject[0]));
            if (cancelled.get()) return;
            for ( IndexResult r :qs.query(PropertiesIndexer.FIELD, text, searchType2Kind(type), PropertiesIndexer.FIELD)) {
                if (cancelled.get()) return;
                for (String v : r.getValues(PropertiesIndexer.FIELD)) {
                    if (cancelled.get()) return;
                    if (matches(v, type, text)) {
                        result.addResult(new PropertiesSymbolProvider.SymbolDescriptorImpl(v, r.getFile()));
                    }
                }
            }
        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        }

    }

    @Override
    public void cancel() {
        cancelled.compareAndSet(false, true);
    }

    @Override
    public void cleanup() {
    }


  static QuerySupport.Kind searchType2Kind(SearchType type) {
        switch (type) {
            case CAMEL_CASE : return QuerySupport.Kind.CAMEL_CASE;
            case PREFIX : return QuerySupport.Kind.PREFIX;
            case EXACT_NAME : return QuerySupport.Kind.EXACT;
            case REGEXP : return QuerySupport.Kind.REGEXP;
            case CASE_INSENSITIVE_EXACT_NAME : return QuerySupport.Kind.EXACT;
            case CASE_INSENSITIVE_PREFIX : return QuerySupport.Kind.CASE_INSENSITIVE_PREFIX;
            case CASE_INSENSITIVE_REGEXP : return QuerySupport.Kind.CASE_INSENSITIVE_REGEXP;
            default: return QuerySupport.Kind.EXACT;
        }
    }
    static Collection<FileObject> getIndexerRoots(Project prj) {
        Collection<FileObject> roots;
        if (prj == null) {
            roots = GlobalPathRegistry.getDefault().getSourceRoots();
        } else {
            ClassPath cp = ClassPath.getClassPath(prj.getProjectDirectory(), ClassPath.SOURCE);
            if (cp != null) {
                roots = Arrays.asList(cp.getRoots());
            } else {
                roots = Collections.emptyList();
            }
        }
        return roots;
    }


    private boolean matches(String v, SearchType type, String text) {
        switch (type) {
            case CASE_INSENSITIVE_EXACT_NAME:
                return v.toLowerCase().equals(text.toLowerCase());
            case CASE_INSENSITIVE_PREFIX:
                return v.toLowerCase().startsWith(text.toLowerCase());
            case EXACT_NAME:
                return v.equals(text);
            case PREFIX:
                return v.startsWith(text);
            case REGEXP:
                return Pattern.matches(text, v);
            case CASE_INSENSITIVE_REGEXP:
                return Pattern.matches(text.toLowerCase(), v.toLowerCase());
            case CAMEL_CASE:
        }
        return false;
    }

    private static class SymbolDescriptorImpl extends SymbolDescriptor {
        private final String propertyText;
        private final FileObject file;
        private final Project project;

        public SymbolDescriptorImpl(String text, FileObject file) {
            this.propertyText = text;
            this.file = file;
            this.project = FileOwnerQuery.getOwner(file);
        }

        @Override
        public Icon getIcon() {
            return null;
        }

        @Override
        public String getSymbolName() {
            return propertyText;
        }

        @Override
        public String getOwnerName() {
            return file.getName();
        }

        @Override
        public String getProjectName() {
            return ProjectUtils.getInformation(project).getDisplayName();
        }

        @Override
        public Icon getProjectIcon() {
            return ProjectUtils.getInformation(project).getIcon();
        }

        @Override
        public FileObject getFileObject() {
            return file;
        }

        @Override
        public int getOffset() {
            //TODO
            return 0;
        }

        @Override
        public void open() {
            try {
                DataObject dob = DataObject.find(file);
                CloneableEditorSupport ces = dob.getLookup().lookup(CloneableEditorSupport.class);
                if (ces != null) {
                    BaseDocument bd = (BaseDocument) ces.openDocument();
                    int off = -1;
                    off = bd.find(new FinderFactory.StringFwdFinder(propertyText + " ", true), 0, bd.getLength());
                    if (off < 0) {
                        off = bd.find(new FinderFactory.StringFwdFinder(propertyText + "=", true), 0, bd.getLength());
                    }
                    off = off >= 0 ? off : 0;
                    showAtOffset(dob, bd, off);
                }
            } catch (DataObjectNotFoundException ex) {
                Exceptions.printStackTrace(ex);
            } catch (IOException | BadLocationException ex) {
                Exceptions.printStackTrace(ex);
            }
        }
    }

    public static void showAtOffset(DataObject dob, BaseDocument bd, int offset) throws IndexOutOfBoundsException {
        try {
            int line = Utilities.getLineOffset(bd, offset);
            int row = Utilities.getRowStart(bd, offset);
            LineCookie lc = dob.getLookup().lookup(LineCookie.class);
            lc.getLineSet().getOriginal(line).show(Line.ShowOpenType.REUSE, Line.ShowVisibilityType.FOCUS, offset - row);
        } catch (BadLocationException ex) {
            Exceptions.printStackTrace(ex);
        }
    }

}
