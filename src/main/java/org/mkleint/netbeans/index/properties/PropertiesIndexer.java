
package org.mkleint.netbeans.index.properties;

import java.io.IOException;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.indexing.Context;
import org.netbeans.modules.parsing.spi.indexing.EmbeddingIndexer;
import org.netbeans.modules.parsing.spi.indexing.Indexable;
import org.netbeans.modules.parsing.spi.indexing.support.IndexDocument;
import org.netbeans.modules.parsing.spi.indexing.support.IndexingSupport;
import org.openide.util.Exceptions;

class PropertiesIndexer extends EmbeddingIndexer {
    static final String FIELD = "key";

    public PropertiesIndexer() {
    }

    @Override
    protected void index(Indexable indexable, Parser.Result parserResult, Context context) {
        if (parserResult instanceof PropertiesParserFactory.PropertiesParserResult) {
            try {
                PropertiesParserFactory.PropertiesParserResult ppr = (PropertiesParserFactory.PropertiesParserResult)parserResult;
                IndexingSupport support = IndexingSupport.getInstance(context);
                IndexDocument doc = support.createDocument(indexable);
                for (String key : ppr.getProps().stringPropertyNames()) {
                    doc.addPair(FIELD, key, true, true);
                }
                support.addDocument(doc);
            } catch (IOException ex) {
                Exceptions.printStackTrace(ex);
            }
        }
    }

}
