
package org.mkleint.netbeans.index.properties;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collection;
import java.util.Properties;
import javax.swing.event.ChangeListener;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.api.Task;
import org.netbeans.modules.parsing.spi.ParseException;
import org.netbeans.modules.parsing.spi.Parser;
import org.netbeans.modules.parsing.spi.ParserFactory;
import org.netbeans.modules.parsing.spi.SourceModificationEvent;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import org.openide.util.Exceptions;

public class PropertiesParserFactory extends ParserFactory {

    @Override
    public Parser createParser(Collection<Snapshot> snapshots) {
        return new PropertiesParser();
    }

    private static class PropertiesParser extends Parser {

        private Result result;

        public PropertiesParser() {
        }

        @Override
        public void parse(Snapshot snapshot, Task task, SourceModificationEvent event) throws ParseException {
            FileObject fo = snapshot.getSource().getFileObject();
            Properties props = new Properties();
            if (fo != null) {
                try {
                    FileLock lock = fo.lock();
                    try {
                        props.load(fo.getInputStream());
                    } finally {
                        lock.releaseLock();
                    }
                } catch (FileNotFoundException ex) {
                    Exceptions.printStackTrace(ex);
                } catch (IOException ex) {
                    Exceptions.printStackTrace(ex);
                }
            }
            this.result = new PropertiesParserResult(snapshot, props);
        }

        @Override
        public Result getResult(Task task) throws ParseException {
            return result;
        }

        @Override
        public void addChangeListener(ChangeListener changeListener) {
        }

        @Override
        public void removeChangeListener(ChangeListener changeListener) {
        }
    }

    public static class PropertiesParserResult extends Parser.Result {
        private final Properties props;

        private PropertiesParserResult(Snapshot snapshot, Properties props) {
            super(snapshot);
            this.props = props;
        }

        public Properties getProps() {
            return props;
        }

        @Override
        protected void invalidate() {
            
        }
    }

}
