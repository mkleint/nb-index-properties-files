
package org.mkleint.netbeans.index.properties;

import java.io.IOException;
import org.netbeans.modules.parsing.api.Snapshot;
import org.netbeans.modules.parsing.spi.indexing.Context;
import org.netbeans.modules.parsing.spi.indexing.EmbeddingIndexer;
import org.netbeans.modules.parsing.spi.indexing.EmbeddingIndexerFactory;
import org.netbeans.modules.parsing.spi.indexing.Indexable;
import org.netbeans.modules.parsing.spi.indexing.support.IndexingSupport;

public class PropertiesIndexerFactory extends EmbeddingIndexerFactory {
    static final String INDEXER_NAME = "property-keys";
    static final int INDEXER_VERSION = 1;

    @Override
    public EmbeddingIndexer createIndexer(Indexable indexable, Snapshot snapshot) {
        return new PropertiesIndexer();
    }

    @Override
    public void filesDeleted(Iterable<? extends Indexable> deleted, Context context) {
        try {
            IndexingSupport is = IndexingSupport.getInstance(context);
            for (Indexable i : deleted) {
                is.removeDocuments(i);
            }
        } catch (IOException ioe) {
        }
    }

    @Override
    public void filesDirty(Iterable<? extends Indexable> dirty, Context context) {
         try {
            IndexingSupport is = IndexingSupport.getInstance(context);
            for (Indexable i : dirty) {
                is.markDirtyDocuments(i);
            }
        } catch (IOException ioe) {
        }
    }

    @Override
    public String getIndexerName() {
        return INDEXER_NAME;
    }

    @Override
    public int getIndexVersion() {
        return INDEXER_VERSION;
    }

}
