# NetBeans module to easily search for location of property keys

* Indexes .properties (mimetype: text/x-properties) files
* Go to Symbol... dialog allows to search for easily jump to the property definitions

Works in NetBeans 8.0+

Download [index-properties-1.0.0.nbm](https://bitbucket.org/mkleint/nb-index-properties-files/downloads/index-properties-1.0.0.nbm) and install using the Tools/Plugins dialog (Downloaded tab)


